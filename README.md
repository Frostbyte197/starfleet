## Installation/Usage Guide ##

### Requirements ###
------------------------------------

To properly use this application, you will need to install several tools and packages which are:

* **Python:** Programming language for back-end
* **Node/NPM:** Package manager to download necessary packages
* **PIP:** Package manager for Python
* **Flask:** Micro-web framework for connecting front-end to back-end
* **Vue:** Framework for front-end
* **Bootstrap:** CSS framework for front-end
* **SQLite:** Database to hold records


### Installation ###
----------------------------
This section is a guide on how to install all the previously mentioned tools and packages required for the application.

#### Python:
Navigate to the [Python website](https://www.python.org/downloads/) and click on "Download Python". After the installation is complete, you can then run.
it and follow the wizard guide through the installation. Accept default settings for all steps in the installation process.

#### Node:
Navigate to the [Node website](https://nodejs.org/en/download/) and download the applicable installer. Once the download is complete, run it and follow the
wizard guide through the setup, accepting default settings for all steps. You can verify that node is installed by opening a command terminal and inputting:
```bash
node -v
```
#### PIP:
To install PIP, open up the command terminal and input the following commands:
```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
```
#### Flask:
To install Flask simply open up a command terminal and input:
```bash
pip install Flask==1.1.2 Flask-Cors==3.0.10
```
#### Vue:
To install Vue open up the command terminal and input:
```bash
npm install -g @vue/cli4.5.11
```
#### Bootstrap:
To install Bootstrap open up the command terminal and input:
```bash
npm install bootstrap@4.6.0 --save
```
#### SQLite:
To install SQLite open up the command terminal and input:
```bash
pip install pysqlite3
```


### Start-Up ###
--------------------
Before we can begin to use the application, it first needs to be downloaded from the BitBucket repository. This is done in the following manner:

* Click on the three horizontal dots at the top-right of the repository.
* Click on 'Download repository'.
* Unzip the downloaded file to a path of your choice.

This application is comprised of both a front-end and a back-end. To run the application, we therefore need to start up both these services. We will begin
by starting up the back-end as follows:

* Open up a command terminal and navigate to the starfleet root folder.
* Run the following command
```bash
python app.py
```

This will run the python script responsible for handling the back-end. Once that is up and running, the front-end can then be started up as well. This is
done as follows:

* Open a new command terminal and navigate to the client folder which is inside the starfleet root folder.
* Input the following command to start up the front-end
```bash
npm run serve
```

Once both the front-end and back-end are up and running, you can open a web browser and navigate to [localhost:8080](http://localhost:8080).

### Usage ###
--------------------
The application is relatively straight-forward to navigate and use. Once you are on the application you will see a data table that displays all the
data records from [https://gorest.co.in/public/v1/users](https://gorest.co.in/public/v1/users). If this is the first time using the application, you will
have to update the table by pressing the 'Update Data' button. This might take a while, depending on how many records are on the data site.
You can track the progress of the download by opening the terminal that is running the Python back-end and looking at the console messages being displayed.

Once all the data is downloaded it will then load into the table and you can navigate through the pages using the 'Next Page'/'Last Page' buttons. If you 
would like to re-download the data, you can press the 'Update Data' button at any time to get the latest data.

To close the application you can simply go to each of the two open command terminals and using the 'Ctrl+C' (for Windows) keyboard shortcut to terminate the
running services.