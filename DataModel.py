import requests
from pathlib import Path
import math

# Global constants
base_Url = "https://gorest.co.in/public/v1/users?page="
data_file_name = "rawData.txt"  # Where host site data is saved to
db_name = "rawData.db"          # Name of sqlite database
db_table_name = 'DATA'
starting_page_number = 1        # Must be a value higher than 0
page_length = 20                # How many records are on a single page
should_verbose = True           # Should display debug log to console?

# Deletes data table if it already exists and creates a new one
def create_table(db_conn):
    cursor = db_conn.cursor()
    cursor.execute('DROP TABLE IF EXISTS {};'.format(db_table_name))

    cursor.execute('''CREATE TABLE {}
            (ID INT PRIMARY KEY   NOT NULL,
            NAME           TEXT   NOT NULL,
            EMAIL          TEXT   NOT NULL,
            GENDER         TEXT   NOT NULL,
            STATUS         TEXT   NOT NULL,
            RESULT         TEXT   NOT NULL);'''.format(db_table_name))

    cursor.close()
    db_conn.commit()
    
# Check if table and database exist
def validate_database(db_con):
    if not Path(db_name).is_file():
        return False

    cursor = db_con.cursor()
    try:
        cursor.execute("SELECT id FROM {}".format(db_table_name)).fetchone()
    except:
        cursor.close()
        return False

    cursor.close()
    return True

# Get specific page from host site and return in JSON-format
def get_page(page_index):
    response = requests.get(base_Url + str(page_index))
    json_response = response.json()['data']
    return json_response

# Adjust data by adding a new 'result' (id / id - 5) field to it
def adjust_data(input_data):
    for i in range(0, len(input_data)):
        current_id = input_data[i]['id']
        current_id = current_id / (current_id - 5)
        input_data[i]['result'] = current_id

    return input_data

# Calculate how many page's worth of data there is in the database
def get_page_count(db_conn):
    if not validate_database(db_conn):
        return 0 

    cursor = db_conn.cursor()
    page_count = len(cursor.execute("SELECT * FROM {}".format(db_table_name)).fetchall())
    cursor.close()
    page_count = math.ceil(page_count / page_length)

    return page_count

# Download all data from host site and save it to database
def download_data(db_conn):
    # Create new table to ensure table only holds updated information
    create_table(db_conn)

    response = requests.get(base_Url + str(starting_page_number))
    page_count = response.json()['meta']['pagination']['pages']
    if should_verbose:
        print(str(page_count) + " pages to download!")

    raw_data = []

    # Download all data to a list
    for i in range(0, page_count):
        if should_verbose:
            print("Downloading page " + str(i + starting_page_number) + "...")

        data_to_add = get_page(i + starting_page_number)
        data_to_add = adjust_data(data_to_add)
        raw_data = raw_data + data_to_add

    # Push list to database
    cursor = db_conn.cursor()
    for i in range(0, len(raw_data)):
        params = (raw_data[i]['id'], raw_data[i]['name'], raw_data[i]['email'], raw_data[i]['gender'], raw_data[i]['status'], raw_data[i]['result'])
        cursor.execute("INSERT INTO {} VALUES (?, ?, ?, ?, ?, ?)".format(db_table_name), params)
        db_conn.commit()

    cursor.close()
    if should_verbose:
        print(str(len(raw_data)) + " records added!")

# Load a page worth of records from database
def load_page(page_index, db_conn):
        start_record = page_length * (page_index - 1)
        end_record = start_record + page_length

        if not validate_database(db_conn):
            return []

        cursor = db_conn.cursor()
        
        # Make sure we don't go out of bounds
        record_count = len(cursor.execute("SELECT * FROM {}".format(db_table_name)).fetchall())
        if end_record > record_count:
            end_record = record_count

        # Retrieve records from database
        params = (start_record, end_record)
        db_select_query = "SELECT id, name, email, gender, status, result FROM {} limit ?, ?".format(db_table_name)
        db_data = cursor.execute(db_select_query, params).fetchall()
        cursor.close()

        return db_data
