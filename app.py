from flask import Flask, jsonify
from flask_cors import CORS
import DataModel
import sqlite3

# Configuration
DEBUG = True

# Instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# Enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

# Route to request back-end to download data
@app.route('/download', methods=['GET'])
def download_data():
    db_conn = sqlite3.connect(DataModel.db_name)
    DataModel.download_data(db_conn)
    db_conn.close()

    return jsonify({
        'success': 'success'
    })

# Route to GET specific page of data
@app.route('/datatable/<page>', methods=['GET'])
def load_all_data(page):
    state = 'success'
    db_conn = sqlite3.connect(DataModel.db_name)
    json_data = DataModel.load_page(int(page), db_conn)
    page_count = DataModel.get_page_count(db_conn)
    db_conn.close()

    # If list is empty, then there is no data and retrieval failed
    if len(json_data) == 0:
        state = 'noData'

    return jsonify({
        'records': json_data,
        'count': page_count,
        'state': state
    })

if __name__ == '__main__':
    app.run()