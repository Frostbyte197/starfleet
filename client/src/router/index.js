import Vue from 'vue';
import VueRouter from 'vue-router';
import DataTable from '../components/DataTable.vue';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'DataTable',
      component: DataTable,
    },
  ],
});
